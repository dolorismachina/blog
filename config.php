<?php
// Connect to database or display error message.
@$db = new mysqli('localhost', 'bloguser', 'bloguserpassword', 'blog');
if ($db->connect_errno)
{
	echo 'Could not establish a connection to the database<br />';
	echo $db->connect_error;
	exit();
}

session_start();

function sanitize($string)
{
	$string = strip_tags($string);
	$string = stripslashes($string);
	$string = htmlentities($string);
	
	return $string;
}

function mysqlString($string)
{
	return mysql_real_escape_string($string);
}

function loggedIn()
{
	if (isset($_SESSION['username']))
		return true;
	else
		return false;
}
?>